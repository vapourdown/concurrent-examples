package org.example;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CyclicBarrierTest {


    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
        //创建CyclicBarrier对象并设置3个公共屏障点,当有三个线程到达就执行任务
        final CyclicBarrier cb = new CyclicBarrier(3,new UserService());
        for (int i = 0; i < 5; i++) {
            Runnable runnable = new Runnable() {
                public void run() {
                    try {
                        cb.await();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            service.execute(runnable);
        }
        service.shutdown();
    }

}
