package org.example;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class StudySemaphoreTest {


    public static void main(String[] args) {

        ExecutorService executorService = Executors.newCachedThreadPool();

        //定义两个令牌数，每个线程获取令牌，如果没有令牌就进行阻塞，等待其他线程释放令牌
        final Semaphore semaphore = new Semaphore(2);

        for (int i=0;i<100;i++){

            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        //获取许可
                        semaphore.acquire();
                        //执行
                        System.out.println("执行任务！");
                        Thread.sleep(2000);
                        //释放
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        executorService.shutdown();
    }

}
