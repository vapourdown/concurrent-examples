package org.example;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * ThreadA、ThreadB、ThreadC，ThreadA 用于初始化数据 num，只有当num初始化完成之后再让ThreadB和ThreadC获取到初始化后的变量num。
 *
 * 考虑到多线程的不确定性，因此我们不能确保ThreadA就一定先于ThreadB和ThreadC前执行，就算ThreadA先执行了，我们也无法保证ThreadA什么时候才能将变量num给初始化完成。因此我们必须让ThreadB和ThreadC去等待ThreadA完成任何后发出的消息。
 * 现在需要解决两个难题，一是让 ThreadB 和 ThreadC 等待 ThreadA 先执行完，二是 ThreadA 执行完之后给ThreadB和ThreadC发送消息。
 */
public class ThreadCommunication {
    private static int num;
    /**
     *  定义一个信号量，该类内部维持了多个线程锁，可以阻塞多个线程，释放多个线程，
     *  线程的阻塞和释放是通过 permit 概念来实现的
     *  线程通过 semaphore.acquire()方法获取 permit，如果当前 semaphore 有 permit 则分配给该线程，
     *  如果没有则阻塞该线程直到 semaphore
     *  调用 release（）方法释放 permit。
     *  构造函数中参数：permit（允许） 个数，
     */

    //这里初始为0一个都不允许
    private static Semaphore semaphore = new Semaphore(0);

    /*
     * 使用countDownLatch实现
     * 定义计数器，初始化1
     * 当线程A执行完了，计数器减一
     * 当线程A没有执行完，其他线程await阻塞
     */
//    private static CountDownLatch countDownLatch = new CountDownLatch(1);

    /**
     * 使用循环屏障，当3个线程都到达屏障的时候就执行
     * 这个有点随机取巧的意思
     */
//    static CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

    public static void main(String[] args) {

        Thread threadA = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    //模拟耗时操作之后初始化变量 num
                    Thread.sleep(1000);
                    num = 1;
                    //初始化完参数后释放两个 permit
                    //因为初始化为0,所以都会阻塞，知道有信号为止
                    //这里释放了两个信号，表示线程A执行完成了
                    semaphore.release(2);
//                    cyclicBarrier.await();
//                    countDownLatch.countDown();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread threadB = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    //获取 permit，如果 semaphore 没有可用的 permit 则等待，如果有则消耗一个
                    semaphore.acquire();
//                    countDownLatch.await();
//                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "获取到 num 的值为：" + num);
            }
        });
        Thread threadC = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    //获取 permit，如果 semaphore 没有可用的 permit 则等待，如果有则消耗一个
                    semaphore.acquire();

//                    countDownLatch.await();

//                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "获取到 num 的值为：" + num);
            }
        });
        //同时开启 3 个线程
        threadA.start();
        threadB.start();
        threadC.start();

    }
}
