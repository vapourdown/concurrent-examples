package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExcutorDemo {


    public static void main(String[] args) {

        //demo 每次都New的线程池，线程会暴增
        for(int i=0;i<100;i++){

            new Thread(() -> {

                List<List<String>> groupList = new ArrayList<>();

                List<String> a = new ArrayList<>();
                a.add("a");
                a.add("b");
                a.add("c");

                List<String> b = new ArrayList<>();
                a.add("d");
                a.add("e");


                groupList.add(a);
                groupList.add(b);


                //获取最大线程数
                int threadNum = 2;
                //获取线程结束超时时间
                int timeOut = 1000;

                //手动创建线程池
                ExecutorService executor = new ThreadPoolExecutor(threadNum, threadNum, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());

                List<String> dataList = new ArrayList<>();

                for(List<String> list : groupList){
                    executor.execute(new UserTask(dataList,list));
                }

                //shutdown()方法的作用是：停止接收新任务，原来的任务继续执行
                executor.shutdown();
                try {

                    /*
                     * 使当前线程阻塞，直到：
                     *
                     * 等所有已提交的任务（包括正在跑的和队列中等待的）执行完
                     * 或者 等超时时间到了（timeout 和 TimeUnit设定的时间）
                     * 或者 线程被中断，抛出InterruptedException
                     */

                    while (!executor.awaitTermination(timeOut, TimeUnit.SECONDS)) {

                        //然后会监测 ExecutorService 是否已经关闭，返回true（shutdown请求后所有任务执行完毕）或false（已超时）
                        // 最后调用shutdownNow()方法，停止接收新任务，原来的任务停止执行
                        executor.shutdownNow();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

//                System.out.println(dataList);



            }).start();

        }

    }



}
