package org.example;

import java.util.List;

public class UserTask implements Runnable{

    private List<String> dataList;

    private List<String> queryList;

    public UserTask(List<String> dataList, List<String> queryList) {
        this.dataList = dataList;
        this.queryList = queryList;
    }

    @Override
    public void run() {

        this.dataList.addAll(queryList);

        System.out.println(Thread.currentThread().getId());

    }
}
