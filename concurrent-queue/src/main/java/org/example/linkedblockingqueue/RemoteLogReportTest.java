package org.example.linkedblockingqueue;


public class RemoteLogReportTest {


    public static void main(String[] args) {

        for(int i=0;i<100;i++){
            new Thread(()-> RemoteLogReport.getInstance().report(Thread.currentThread().getId()+"")).start();
        }

        //因为RemoteLogReport创建的是守护线程，所以这里让main线程沉睡10秒，让守护线程有机会执行
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
