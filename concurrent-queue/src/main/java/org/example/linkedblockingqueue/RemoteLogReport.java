package org.example.linkedblockingqueue;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 远程日志上报
 *
 * 开起固定守护线程，消费阻塞队列
 */
public class RemoteLogReport {


    private LinkedBlockingQueue<String> queue;
    private ExecutorService executorService;

    private RemoteLogReport() {

        this.queue = new LinkedBlockingQueue<>(1000);

        executorService = Executors.newFixedThreadPool(3, r -> {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            return t;
        });
        //线程池中有三个线程，开起三个
        for(int i=0;i<3;++i){

            executorService.execute(() -> {

                while (true){
                    try {
                        String msg = queue.take();
                        System.out.println("线程"+Thread.currentThread().getId()+"->消费消息："+msg);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    public void report(String msg){
        queue.offer(msg);
    }

    private static class HolderClass{
        private final static  RemoteLogReport instance = new RemoteLogReport();
    }
    public static RemoteLogReport getInstance(){
        return HolderClass.instance;
    }

}
