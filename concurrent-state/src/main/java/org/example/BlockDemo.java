package org.example;

import java.util.concurrent.TimeUnit;

public class BlockDemo extends Thread{


    @Override
    public void run(){

        synchronized (BlockDemo.class){
            while (true){

                try {
                    TimeUnit.SECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    }

}
