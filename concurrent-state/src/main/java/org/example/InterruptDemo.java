package org.example;

import java.util.concurrent.TimeUnit;

/**
 * interrupt()的作用
 * 设置一个共享变量的值 true
 * 唤醒处于阻塞状态下的线程
 */
public class InterruptDemo implements Runnable{

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){

            try {
                TimeUnit.SECONDS.sleep(200);
            } catch (InterruptedException e) {//会触发线程复位， interrupt = false
                e.printStackTrace();
                //可以不做处理，
                //继续中断 ->
                Thread.currentThread().interrupt(); //再次中断
                //抛出异常。。
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Thread thread  = new Thread(new InterruptDemo());
        thread.start();

        Thread.sleep(1000);

        thread.interrupt();//设置 interrupted=true;
    }

}
