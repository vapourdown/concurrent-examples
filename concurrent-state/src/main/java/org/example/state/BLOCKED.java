package org.example.state;

/**
 * @see Thread.State.BLOCKED
 *
 * 等待监视锁，这个时候线程被操作系统挂起。当进入synchronized块/方法或者在调用wait()被唤醒/超时之后重新进入synchronized块/方法，
 * 锁被其它线程占有，这个时候被操作系统挂起，状态为阻塞状态。
 * 阻塞状态的线程，即使调用interrupt()方法也不会改变其状态。
 */
public class BLOCKED {

    public static void main(String[] args) {
        testBlockedState();
    }

    //模拟两个线程抢锁，当一个线程抢到锁之后进入sleep，sleep状态下不会释放锁，
    //所以另外一个线程被阻塞。从堆栈信息可以看到，locked和waiting to lock都是同一个对象。
    public static void testBlockedState() {

        Object lock = new Object();
        SleepThread t1 = new SleepThread("t1", lock);
        SleepThread t2 = new SleepThread("t2", lock);
        t1.start();
        t2.start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Thread t1's state " + t1.getState());
        System.out.println("Thread t2's state " + t2.getState());
    }

    static class SleepThread extends Thread {

        private final String name;
        private final Object lock;

        public SleepThread(String name, Object lock) {
            super(name);
            this.name = name;
            this.lock = lock;
        }

        @Override
        public void run() {
            System.out.println("Thread:" + name + " in run.");

            synchronized (lock) {
                System.out.println("Thread:" + name + " hold the lock.");

                try {
                    Thread.sleep(1000 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("Thread:" + name + " return the lock.");
            }
        }
    }

}
