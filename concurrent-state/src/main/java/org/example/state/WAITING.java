package org.example.state;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @see Thread.State.WAITING
 *
 * 无条件等待，当线程调用wait()/join()/LockSupport.park()不加超时时间的方法之后所处的状态，
 * 如果没有被唤醒或等待的线程没有结束，那么将一直等待，当前状态的线程不会被分配CPU资源和持有锁。
 */
public class WAITING {


    public static void main(String[] args) {
        testStateWatingByWait();
        testStateWatingByJoin();
        testStateWatingByThreadExecutor();
    }

    /**
     * 线程调用wait方法，状态变成WAITING。
     */
    public static void testStateWatingByWait() {

        Object lock = new Object();
        WaitingThread waitingThread = new WaitingThread("WaitingThread", lock);
        waitingThread.start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Main thread check the state is " + waitingThread.getState() + "."); // WAITING
    }

    static class WaitingThread extends Thread {
        private final int timeout;
        private final Object lock;

        public WaitingThread(String name, Object lock) {
            this(name, lock, 0);
        }

        public WaitingThread(String name, Object lock, int timeout) {
            super(name);
            this.timeout = timeout;
            this.lock = lock;
        }

        @Override
        public void run() {
            synchronized (lock) {
                if (timeout == 0) {
                    try {
                        System.out.println("Try to wait.");
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        System.out.println("Try to wait in " + timeout + ".");
                        lock.wait(timeout);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            System.out.println("Over thread.");
        }
    }


    /**
     * 线程调用join方法，状态变成WAITING。
     */
    public static void testStateWatingByJoin() {

        Object lock = new Object();
        WaitingThread waitingThread = new WaitingThread("WaitingThread", lock);
        waitingThread.start();
        JoinThread joinThread = new JoinThread("JoinThread", waitingThread);
        joinThread.start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Main thread check the join thread's state is " + joinThread.getState() + "."); // WAITING
    }

    static class JoinThread extends Thread {
        private int timeout = 0;
        private Thread thread;

        public JoinThread(String name, Thread thread) {
            this(name, thread, 0);
        }

        public JoinThread(String name, Thread thread, int timeout) {
            super(name);
            this.timeout = timeout;
            this.thread = thread;
        }

        @Override
        public void run() {
            if (timeout == 0) {
                try {
                    System.out.println("Try to join.");
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    System.out.println("Try to join in " + timeout + ".");
                    thread.join(timeout);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Over join.");
        }
    }



    //调用LockSupport.park方法导致的WAITING状态。
    //使用线程池的时候经常会遇到这种状态，当线程池里面的任务都执行完毕，会等待获取任务。
    public static void testStateWatingByThreadExecutor() {

        ExecutorService executeService = Executors.newSingleThreadExecutor();
        executeService.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("Over Run.");
            }
        });

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
