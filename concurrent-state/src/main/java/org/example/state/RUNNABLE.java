package org.example.state;


import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @see Thread.State.RUNNABLE
 *
 * 当线程被调用了start()，且处于等待操作系统分配资源（如CPU）、等待IO连接、正在运行状态，即表示Running状态和Ready状态。
 * 注：不一定被调用了start()立刻会改变状态，还有一些准备工作，这个时候的状态是不确定的。
 *
 */

public class RUNNABLE {



    public static void main(String[] args) {
        testStateRunnable();
    }


    //进入run方法之后线程的状态，以及在等待IO的时候，线程的状态
    public static void testStateRunnable() {
        IOThread simpleThread = new IOThread("IOThread");
        simpleThread.setName("IOThread");
        simpleThread.start();


        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Main thread check the state is " + simpleThread.getState() + "."); // RUNNABLE
    }

    static class IOThread extends Thread {

        public IOThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            System.out.println("In run method, state is " + getState() + "."); // RUNNABLE
            Socket socket = new Socket();
            try {
                System.out.println("Try to connect socket address which not exist...");
                socket.connect(new InetSocketAddress(
                        InetAddress.getByAddress(new byte[] { (byte) 192, (byte) 168, 1, 14 }), 5678));
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
