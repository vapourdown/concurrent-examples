package org.example.state;

/**
 * @see Thread.State.TERMINATED
 *
 * 执行完了run()方法。其实这只是Java语言级别的一种状态，在操作系统内部可能已经注销了相应的线程，
 * 或者将它复用给其他需要使用线程的请求，而在Java语言级别只是通过Java代码看到的线程状态而已。
 */
public class TERMINATED {

    public static void main(String[] args) {
        testNewAndTerminatedState();
    }

    public static void testNewAndTerminatedState() {

        Thread thread = new Thread(() -> System.out.println("Over Run."));

        System.out.println("State " + thread.getState() + ".");
        thread.start();

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("State " + thread.getState() + ".");
    }

}
