package org.example.state;

/**
 * @see Thread.State.TIMED_WAITING
 *
 * 有条件的等待，当线程调用sleep(睡眠时间)/wait(等待时间)/join(等待时间)/
 * LockSupport.parkNanos(等待时间)/LockSupport.parkUntil(等待时间)方法之后所处的状态，
 * 在指定的时间没有被唤醒或者等待线程没有结束，会被系统自动唤醒，正常退出。
 */
public class TIMED_WAITING {

    public static void main(String[] args) {
        testSleep();
    }

    public static void testSleep() {
        try {
            Thread.sleep(1000 * 100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
