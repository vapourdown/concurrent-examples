package org.example;


import java.util.concurrent.TimeUnit;

/**
 * jps 命令 获取java pid
 * jstack pid  查看pid 对于的堆栈信息
 */
public class App {

    //阻塞状态
    public static void block(){

        new Thread(()->{

            while (true){

                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        },"STATUS_BLOCK").start();//TIMED_WAITING


        new Thread(()->{

            while (true) {
                synchronized (App.class){

                    try {
                        App.class.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        },"STATUS_BLOCK_01").start();//WAITING


        new Thread(new BlockDemo(),"BLOCK_DEMO_01").start();//TIMED_WAITING
        new Thread(new BlockDemo(),"BLOCK_DEMO_02").start();//BLOCKED

    }



    public static void main( String[] args ){
        block();
    }
}
