package org.example;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class FutureDemo implements Callable<String> {


    public static void main(String[] args) {

        FutureDemo futureDemo = new FutureDemo();

        FutureTask<String> futureTask = new FutureTask<>(futureDemo);

        new Thread(futureTask).start();

        try {
            System.out.println(futureTask.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String call() throws Exception {
        return "Hello sb";
    }



}
