package org.example;

public class YieldTest implements Runnable{


    public void run() {

        for(int i=0;i<100;i++){
            if(i == 50){
                Thread.yield();
                System.out.println(Thread.currentThread().getId()+"让出时间片!");
            }
        }
    }


    public static void main(String[] args) {

        for(int i=0;i<100;i++){
            new Thread(new YieldTest()).start();
        }

    }

}
