package com.concurrent.simple;



public class RunnableSimple implements Runnable {

    @Override
    public void run() {
        System.out.println("MyThread");
    }


    public static void main(String[] args) {
        new Thread(new RunnableSimple()).start();
    }

}
