package com.concurrent.simple;


public class ThreadSimple extends Thread {

    @Override
    public void run() {
        System.out.println("MyThread");
    }

    public static void main(String[] args) {
        new ThreadSimple().start();
    }
}
