package org.example;

public class VolatileDemo {

    public volatile static boolean stop  = false;

    public static void setDemo(){
        stop = true;
    }

    public static void main(String[] args) {
        setDemo();
    }

}
