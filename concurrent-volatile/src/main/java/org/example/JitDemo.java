package org.example;

public class JitDemo {

    public static boolean stop =false;

    /**
     * 因为println有加锁的操作，而释放锁的操作，会强制性的把工作内存中涉及到的写操作同步到主
     * 内存，可以通过如下代码去证明
     */
    public static void main(String[] args) {
        Thread thread=new Thread(()->{
            int i=0;
                while(!stop){
                    i++;
                    synchronized (ActiveFailDemo.class){
                }
            }
        });
    }
}
