package org.example;

public class ActiveFailDemo {

//    public volatile  static boolean stop =false;
    public volatile static boolean stop =false;

    /**
     * 活性失败. JIT深度优化
     *
     * println底层用到了synchronized这个同步关键字，这个同步会防止循环期间对于stop值的缓存
     *
     */

    public static void main(String[] args) throws InterruptedException {
            Thread thread=new Thread(()->{
                int i=0;
                while(!stop){
                    i++;

                    //此处加System.out.println程序也会结束
                    //此处加Thread.sleep也会借宿
                    //活性失败

                    /*
                     * IO操作
                     * synchronized关键字
                     */
                }
                System.out.println("rs:"+i);
            });
            thread.start();
            Thread.sleep(1000);
            stop=true;
            //可见性问题，在外层设置stop=true，子线程并没有看到变化 (加volatile)
    }

}
