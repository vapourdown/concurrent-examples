package org.example;


public class DLCClass {

    //不完整对象  需要加volatile
    public static  DLCClass dlcClass;

    private DLCClass(){}


    public static DLCClass getInstance(){
        if(dlcClass == null){
            synchronized (dlcClass){
                if(dlcClass == null){

                    //new 操作 在底层有三个指令 这些指令允许重排序
                    dlcClass = new DLCClass();
                }
            }
        }
        return dlcClass;
    }

}
