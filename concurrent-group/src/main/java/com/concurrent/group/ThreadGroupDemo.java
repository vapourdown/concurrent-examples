package com.concurrent.group;


/**
 * Java中用ThreadGroup来表示线程组，我们可以使用线程组对线程进行批量控制。
 *
 * ThreadGroup和Thread的关系就如同他们的字面意思一样简单粗暴，
 * 每个Thread必然存在于一个ThreadGroup中，Thread不能独立于ThreadGroup存在。
 * 执行main()方法线程的名字是main，如果在new Thread时没有显式指定，
 * 那么默认将父线程（当前执行new Thread的线程）线程组设置为自己的线程组。
 */
public class ThreadGroupDemo {


    //获取线程组名字
    private static String getThreadGroupName(){
        return Thread.currentThread().getThreadGroup().getName();
    }

    //复制线程组
    private static void copyThreadGroup(){
        // 获取当前的线程组
        ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
        // 复制一个线程组到一个线程数组（获取Thread信息）
        Thread[] threads = new Thread[threadGroup.activeCount()];
        threadGroup.enumerate(threads);
    }

    public static void main(String[] args) {

        System.out.println(getThreadGroupName());

        System.out.println("执行main所在线程的线程组名字： " + Thread.currentThread().getThreadGroup().getName());
        System.out.println("执行main方法线程名字：" + Thread.currentThread().getName());
    }

}
