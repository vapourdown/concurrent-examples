package org.example;

import java.util.concurrent.TimeUnit;

public class InterruptTest implements Runnable{


    @Override
    public void run() {
        for(int i=0;i<100000;i++){
            System.out.println(i);
            if(Thread.interrupted()){
                System.out.println("线程已经被中断！");
                break;
            }
        }
    }


    public static void main(String[] args) {

      Thread thread =   new Thread(new InterruptTest());
      thread.start();

      try {
          TimeUnit.SECONDS.sleep(1);
      } catch (InterruptedException e) {
          e.printStackTrace();
      }

      thread.interrupt();

    }
}
